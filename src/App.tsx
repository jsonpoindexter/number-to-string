import React from 'react';
import './App.css';
import Input from "./Input";

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <Input />
      </header>
    </div>
  );
};

export default App;
