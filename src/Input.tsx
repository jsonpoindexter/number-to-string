import React, { ChangeEvent, Component } from "react";

interface InputState {
  input: string;
  minInputValue: number;
  maxInputValue: number;
  isInvalidInput: boolean;
  sentence: string;
}

interface NumberToString {
  [key: number]: string;
}

const sentenceStyle = {
  textSizeAdjust: "auto"
};

class Input extends Component<{}, InputState> {
  numberToStringObj: NumberToString = {
    0: "Zero",
    1: "One",
    2: "Two",
    3: "Three",
    4: "Four",
    5: "Five",
    6: "Six",
    7: "Seven",
    8: "Eight",
    9: "Nine",
    10: "Ten",
    11: "Eleven",
    12: "Twelve",
    13: "Thirteen",
    14: "Fourteen",
    15: "Fifteen",
    16: "Sixteen",
    17: "Seventeen",
    18: "Eighteen",
    19: "Nineteen",
    20: "Twenty",
    30: "Thirty",
    40: "Forty",
    50: "Fifty",
    60: "Sixty",
    70: "Seventy",
    80: "Eighty",
    90: "Ninety"
  };

  powersToString: NumberToString = {
    0: "Hundred",
    1: "Thousand",
    2: "Million"
  };

  constructor(props: any) {
    super(props);
    this.state = {
      input: "",
      minInputValue: 0,
      maxInputValue: 999999999,
      isInvalidInput: true,
      sentence: ""
    };
  }

  // Input  1234
  // Index: 0123
  generateNumber = (input: string) => {
    input = input
      .split("")
      .reverse()
      .join("");
    let sentence: string = "";

    for (let index = Math.ceil(input.length / 3) - 1; index >= 0; index--) {
      // index[1] a: NaN b: NaN c: 1
      // index[0] a: 2 b: 3 c: 4
      const a = parseInt(input[index * 3 + 2]);
      const b = parseInt(input[index * 3 + 1]);
      const c = parseInt(input[index * 3]);

      if (!isNaN(a) && a > 0) {
        sentence += this.numberToStringObj[a];
        sentence += ` ${this.powersToString[0]} `;
      }

      if (b + c !== 0) sentence += this.numberToString(b, c);

      // Handle add Hundred, Thousand, Million..
      if (index > 0 && (a > 0 || b > 0 || c > 0)) {
        sentence += `${this.powersToString[index]} `;
      }
    }
    this.setState({
      sentence
    });
  };

  numberToString = (b: number, c: number): string => {
    if (isNaN(b)) {
      return `${this.numberToStringObj[c]} `;
    } else {
      if (b === 0) return `${this.numberToStringObj[c]}`;
      else if (c === 0) return `${this.numberToStringObj[b * 10]} `;
      else if (b === 1) return `${this.numberToStringObj[b * 10 + c]} `;
      else
        return `${this.numberToStringObj[b * 10]}-${
          this.numberToStringObj[c]
        } `;
    }
  };

  inputDidChange = (event: ChangeEvent<HTMLInputElement>) => {
    let input = event.target.value;
    if (
      parseInt(input) > this.state.maxInputValue ||
      parseInt(input) < this.state.minInputValue
    ) {
      this.setState({ isInvalidInput: true });
    } else {
      this.setState({
        isInvalidInput: false
      });
      this.generateNumber(input);
    }
    this.setState({
      input: input
    });
  };

  render() {
    return (
      <div>
        {(parseInt(this.state.input) > this.state.maxInputValue ||
          parseInt(this.state.input) < this.state.minInputValue) && (
          <div>
            Input must be between {this.state.minInputValue} and{" "}
            {this.state.maxInputValue}
          </div>
        )}
        <input
          placeholder={`Enter a number between ${
            this.state.minInputValue
          } and ${this.state.maxInputValue}`}
          type={"text"}
          name={"input"}
          onChange={this.inputDidChange}
          className={"input"}
        />
        <div style={sentenceStyle}>{this.state.sentence}</div>
      </div>
    );
  }
}

export default Input;
